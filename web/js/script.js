$(function() {

var StegState = function() {
	this.initEvents();
	this.setState(StegState.State.START);
};
StegState.State = {
	START: "start",
	DRAGDROP: "dragdrop",
	ENCODE_STEP: "encode_step",
	DOWNLOAD: "download",
	UPLOADING: "uploading"
};
StegState.prototype = {
	state: null,
	checkpointState: null,

	initEvents: function() {
		var self = this;

		$('#encode_upload_choose').on('change', function() {
			self.setState(StegState.State.UPLOADING);
			// do upload
			// progress to State.ENCODE_STEP
			// on failure, progress to State.START
		});
		$('#decode_upload_choose').on('change', function() {
			self.setState(StegState.State.UPLOADING);
			// do upload
			// progress to State.DOWNLOAD
			// on failure, progress to State.START
		});
		$('#encode_dragdrop').on('click', function() {
			self.setState(StegState.State.UPLOADING);
			// do upload
			// progress to State.ENCODE_STEP
			// on failure, progress to State.START
		});
		$('#decode_dragdrop').on('click', function() {
			self.setState(StegState.State.UPLOADING);
			// do upload
			// progress to State.DOWNLOAD
			// on failure, progress to State.START
		});
		$('#payload_upload_choose').on('change', function() {
			self.setState(StegState.State.UPLOADING);
			// do upload
			// progress to State.DOWNLOAD
			// on failure, progress to State.ENCODE_STEP
		});
		$('#download_link').on('click', function() {
			// initiate download
		});
	},

	setState: function(state) {
		$('section[data-state]').hide();
		$('section[data-state='+state+']').show();
		switch (state) {
			case StegState.State.START:
				this.checkpointState = state;
				break;
			case StegState.State.DRAGDROP:
				this.checkpointState = this.state;
				break;
			case StegState.State.ENCODE_STEP:
				this.checkpointState = state;
				break;
			case StegState.State.DOWNLOAD:
				this.checkpointState = state;
				break;
			case StegState.State.UPLOADING:
				this.checkpointState = this.state;
				break;
		}
		this.state = state;
	}
};

var state = new StegState();

});

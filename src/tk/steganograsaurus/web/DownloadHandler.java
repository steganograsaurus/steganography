package tk.steganograsaurus.web;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.NanoHTTPD.IHTTPSession;
import fi.iki.elonen.NanoHTTPD.Response;
import fi.iki.elonen.NanoHTTPD.Response.Status;
import fi.iki.elonen.router.RouterNanoHTTPD.UriResource;
import fi.iki.elonen.router.RouterNanoHTTPD.UriResponder;

import tk.steganograsaurus.steg.FileData;
import tk.steganograsaurus.util.WebServer;

public class DownloadHandler implements UriResponder {
	public DownloadHandler() {
	}

	public Response get(UriResource uriResource, Map<String, String> urlParams, IHTTPSession session) {
		String storageDirectory = WebServer.UPLOAD_DIR;
		try {
			String fileId = urlParams.get("id");
			File file = new File(storageDirectory, fileId);
			FileData fileData = FileData.fromFile(file);
			byte[] bytes = fileData.getByteStream();
			InputStream stream = new ByteArrayInputStream(bytes);

			return NanoHTTPD.newFixedLengthResponse(Status.OK, fileData.getMimetype(), stream, bytes.length);
		} catch (IOException e) {
			return NanoHTTPD.newFixedLengthResponse(Status.NOT_FOUND, "text/plain", "");
		}
	}

	public Response put(UriResource uriResource, Map<String, String> urlParams, IHTTPSession session) {
		return NanoHTTPD.newFixedLengthResponse(Status.METHOD_NOT_ALLOWED, "text/plain", "");
	}

	public Response delete(UriResource uriResource, Map<String, String> urlParams, IHTTPSession session) {
		return NanoHTTPD.newFixedLengthResponse(Status.METHOD_NOT_ALLOWED, "text/plain", "");
	}

	public Response other(String method, UriResource uriResource, Map<String, String> urlParams, IHTTPSession session) {
		return NanoHTTPD.newFixedLengthResponse(Status.METHOD_NOT_ALLOWED, "text/plain", "");
	}

	public Response post(UriResource uriResource, Map<String, String> urlParams, IHTTPSession session) {
		return NanoHTTPD.newFixedLengthResponse(Status.METHOD_NOT_ALLOWED, "text/plain", "");
	}



}

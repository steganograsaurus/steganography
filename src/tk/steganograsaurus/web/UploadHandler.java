package tk.steganograsaurus.web;

import java.util.List;
import java.util.Map;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;

import fi.iki.elonen.NanoFileUpload;
import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.NanoHTTPD.IHTTPSession;
import fi.iki.elonen.NanoHTTPD.Response;
import fi.iki.elonen.NanoHTTPD.Response.Status;
import fi.iki.elonen.router.RouterNanoHTTPD.UriResource;
import fi.iki.elonen.router.RouterNanoHTTPD.UriResponder;

public abstract class UploadHandler implements UriResponder {
	public UploadHandler() {
	}

	protected FileItem processUpload(IHTTPSession session, NanoFileUpload uploader) throws FileUploadException {
		List<FileItem> items = uploader.parseRequest(session);
		for (FileItem item : items) {
			if (!item.isFormField()) {
				return item;
			}
		}
		throw new FileUploadException();
	}

	public Response get(UriResource uriResource, Map<String, String> urlParams, IHTTPSession session) {
		return NanoHTTPD.newFixedLengthResponse(Status.METHOD_NOT_ALLOWED, "text/plain", "");
	}

	public Response put(UriResource uriResource, Map<String, String> urlParams, IHTTPSession session) {
		return NanoHTTPD.newFixedLengthResponse(Status.METHOD_NOT_ALLOWED, "text/plain", "");
	}

	public Response delete(UriResource uriResource, Map<String, String> urlParams, IHTTPSession session) {
		return NanoHTTPD.newFixedLengthResponse(Status.METHOD_NOT_ALLOWED, "text/plain", "");
	}

	public Response other(String method, UriResource uriResource, Map<String, String> urlParams, IHTTPSession session) {
		return NanoHTTPD.newFixedLengthResponse(Status.METHOD_NOT_ALLOWED, "text/plain", "");
	}

	public Response post(UriResource uriResource, Map<String, String> urlParams, IHTTPSession session) {
		return NanoHTTPD.newFixedLengthResponse(Status.METHOD_NOT_ALLOWED, "text/plain", "");
	}

}

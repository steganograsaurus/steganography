package tk.steganograsaurus.web;

import java.io.File;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;

import fi.iki.elonen.NanoFileUpload;
import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.NanoHTTPD.IHTTPSession;
import fi.iki.elonen.NanoHTTPD.Response;
import fi.iki.elonen.NanoHTTPD.Response.Status;
import fi.iki.elonen.router.RouterNanoHTTPD.UriResource;

import tk.steganograsaurus.steg.FileData;
import tk.steganograsaurus.steg.ISteg;
import tk.steganograsaurus.util.WebServer;

public class EncodeHandler extends UploadHandler {
	public EncodeHandler() {
		super();
	}

	public Response post(UriResource uriResource, Map<String, String> urlParams, IHTTPSession session) {
		NanoFileUpload uploader = uriResource.initParameter(NanoFileUpload.class);
		String storageDirectory = WebServer.UPLOAD_DIR;
		try {
			FileItem file = this.processUpload(session, uploader);
			String fileId = UUID.randomUUID().toString();
			File stored = new File(storageDirectory, fileId);
			file.write(stored);

			FileData data = FileData.fromFile(stored);
			ISteg steg = ISteg.buildSteg(data);

			return NanoHTTPD.newFixedLengthResponse(Status.OK, "application/json",
				String.format("{\"id\":\"%s\",\"capacity\":%d}", fileId, steg.capacity())
			);
		} catch (FileUploadException e) {
			return NanoHTTPD.newFixedLengthResponse(Status.BAD_REQUEST, "text/plain", "File upload exception: " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			return NanoHTTPD.newFixedLengthResponse(Status.BAD_REQUEST, "text/plain", "General error");
		}
	}
}

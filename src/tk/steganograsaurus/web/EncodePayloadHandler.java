package tk.steganograsaurus.web;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.Writer;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;

import fi.iki.elonen.NanoFileUpload;
import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.NanoHTTPD.IHTTPSession;
import fi.iki.elonen.NanoHTTPD.Response;
import fi.iki.elonen.NanoHTTPD.Response.Status;
import fi.iki.elonen.router.RouterNanoHTTPD.UriResource;

import tk.steganograsaurus.steg.FileData;
import tk.steganograsaurus.steg.ISteg;
import tk.steganograsaurus.util.WebServer;

public class EncodePayloadHandler extends UploadHandler {
	public EncodePayloadHandler() {
		super();
	}

	public Response post(UriResource uriResource, Map<String, String> urlParams, IHTTPSession session) {
		NanoFileUpload uploader = uriResource.initParameter(NanoFileUpload.class);
		String storageDirectory = WebServer.UPLOAD_DIR;
		try {
			String fileId = urlParams.get("id");
			File container = new File(storageDirectory, fileId);
			FileData containerData = FileData.fromFile(container);

			FileItem payload = this.processUpload(session, uploader);
			FileData payloadData = FileData.fromBytes(payload.get());

			ISteg steg = ISteg.buildSteg(containerData);

			FileData encodedData = steg.encode(payloadData);
			String encodedId = UUID.randomUUID().toString();
			File encoded = new File(storageDirectory, encodedId);

			FileOutputStream out = new FileOutputStream(encoded);
			out.write(encodedData.getByteStream());
			out.close();

			return NanoHTTPD.newFixedLengthResponse(Status.OK, "application/json",
				String.format("{\"id\":\"%s\"}", encodedId)
			);
		} catch (FileUploadException e) {
			return NanoHTTPD.newFixedLengthResponse(Status.BAD_REQUEST, "text/plain", "File upload exception: " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			return NanoHTTPD.newFixedLengthResponse(Status.BAD_REQUEST, "text/plain", "General error");
		}
	}
}

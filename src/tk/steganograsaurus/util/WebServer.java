package tk.steganograsaurus.util;

import java.io.IOException;

import org.apache.commons.fileupload.disk.DiskFileItemFactory;

import fi.iki.elonen.router.RouterNanoHTTPD;
import fi.iki.elonen.util.ServerRunner;

import tk.steganograsaurus.web.DecodeHandler;
import tk.steganograsaurus.web.DownloadHandler;
import tk.steganograsaurus.web.EncodeHandler;
import tk.steganograsaurus.web.EncodePayloadHandler;
import fi.iki.elonen.NanoFileUpload;

public class WebServer extends RouterNanoHTTPD {
	private final static int PORT = 8080;
	public static final String UPLOAD_DIR = "/tmp";

	public WebServer() throws IOException {
		super(WebServer.PORT);
		addMappings();
	}

	@Override
	public void addMappings() {
		super.addMappings();
		NanoFileUpload uploader = new NanoFileUpload(new DiskFileItemFactory());
		addRoute("/encode", EncodeHandler.class, uploader);
		addRoute("/encode/:id", EncodePayloadHandler.class, uploader);
		addRoute("/decode", DecodeHandler.class, uploader);
		addRoute("/download/:id", DownloadHandler.class);
	}

	public static void main(String[] args) {
		ServerRunner.run(WebServer.class);
	}
}

package tk.steganograsaurus.util;

import tk.steganograsaurus.steg.FileData;
import tk.steganograsaurus.steg.ISteg;

import java.io.File;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionGroup;
import org.apache.commons.cli.Options;

/**
 * Main entry point for the CLI program
 */
public class CLI {
	public static void main(String[] args) {
		Options options = new Options();

		options.addOption("h", "help", false, "print this help");

		OptionGroup modeOption = new OptionGroup();
		modeOption.setRequired(true);
		modeOption.addOption(Option.builder("e")
				.longOpt("encode")
				.numberOfArgs(2)
				.argName("container-file payload-file")
				.valueSeparator(' ')
				.desc("encode payload data into a container")
				.build()
		);
		modeOption.addOption(Option.builder("d")
				.longOpt("decode")
				.numberOfArgs(1)
				.argName("file")
				.desc("decode file to retrieve payload data")
				.build()
		);
		modeOption.addOption(Option.builder("c")
				.longOpt("capacity")
				.numberOfArgs(1)
				.argName("file")
				.desc("calculate the payload capacity of the given container file, in bytes")
				.build()
		);

		options.addOptionGroup(modeOption);
		HelpFormatter help = new HelpFormatter();

		try {
			CommandLineParser parser = new DefaultParser();
			CommandLine cmd = parser.parse(options, args);

			if (cmd.hasOption("help")) {
				help.printHelp("steg", options, true);
				return;
			}

			switch (modeOption.getSelected()) {
				case "e": {
					String[] opts = cmd.getOptionValues("encode");
					FileData container = FileData.fromFile(new File(opts[0]));
					FileData payload = FileData.fromFile(new File(opts[1]));
					ISteg steg = ISteg.buildSteg(container);

					FileData result = steg.encode(payload);
					System.out.write(result.getByteStream());
				}
				break;

				case "d": {
					String[] opts = cmd.getOptionValues("decode");
					FileData container = FileData.fromFile(new File(opts[0]));
					ISteg steg = ISteg.buildSteg(container);

					FileData result = steg.decode();
					System.out.write(result.getByteStream());
				}
				break;

				case "c": {
					String[] opts = cmd.getOptionValues("decode");
					FileData container = FileData.fromFile(new File(opts[0]));
					ISteg steg = ISteg.buildSteg(container);

					System.out.format("%d%n", steg.capacity());
				}
				break;

				default: {
					System.err.println("Something went wrong...");
					return;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			help.printHelp("steg", options, true);
		}
	}
}

package tk.steganograsaurus.steg;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Random;

/**
 * Created by Adomas on 2016-01-30.
 */
public class FileDataForImage extends FileData {

    byte[] dataBytes;
    int[] dataInts;
    byte[] dataBytesBoolean;
    String dataString;

    byte[] dataOutBytes;

    int[] chunkMarker = new int[]{1, 0, 1, 0, 1, 0, 1, 0};
    int chunkMarkerLength = 8;
    int chunkPacketNumberLength = 8;
    int chunkDataLength = 32;
    int chunkLength = chunkMarkerLength + chunkPacketNumberLength * 2 + chunkDataLength * 2;
    int chunkRepetition = 1;

    int dataCapacity = 20000;

    public FileDataForImage(byte[] data, int dataCapacity) {
        super(data, "");
        dataBytes = data;
        this.data = data;
        this.dataCapacity = dataCapacity;

        //makeMessage();
    }

    void makeMessage() {
//        dataInts = bytesToInts(dataBytes);
//        dataString = IntsToString(dataInts);
//        dataOutBytes = intsToBytes(dataInts);
//
//        int[] composedStuff = compose(dataInts);
//
//        composedStuff = removeSomeStuff(composedStuff, 0);
//
//        byte[] retrievedArray = intsToBytes(decompose(composedStuff));
//

    }

    public void Encode() {
        dataInts = bytesToInts(dataBytes);
        int[] composedStuff = compose(dataInts);
        dataBytesBoolean = new byte[composedStuff.length];
        for (int i = 0; i < composedStuff.length; i++) {
            dataBytesBoolean[i] = (byte) composedStuff[i];
        }
        data = dataBytesBoolean;
    }

    public FileData Decode() {
        int[] composedStuff = new int[data.length];

        for (int i = 0; i < data.length; i++) {
            composedStuff[i] = data[i];
        }

        byte[] retrievedArray = intsToBytes(decompose(composedStuff));
        return new FileData(retrievedArray, "image/png");
    }

    int[] removeSomeStuff(int[] data, int ammount) {
//        Random random = new Random();
//        for (int i = 0; i < ammount; i++) {
//            int index = random.nextInt(data.length);
//            data[index] = data[index] == 1 ? 0 : 1;
//        }
//        return data;

        Random random = new Random();
        int[] less = data;
        for (int i = 0; i < ammount; i++) {
            int index = random.nextInt(less.length);
            int[] lessTemp = new int[less.length - 1];
            System.arraycopy(less, 0, lessTemp, 0, index);
            System.arraycopy(less, index, lessTemp, index - 1, less.length - index);
            less = lessTemp;
        }
        return less;
    }

    int[] compose(int[] data) {

        //create 2D array
        int dataLength = data.length / chunkDataLength;
        if (data.length % chunkDataLength != 0) {
            dataLength += 1;
        }
        int[][] dataReady = new int[dataLength][chunkLength];

        //compose in 2D array
        for (byte chunkNumber = 0; chunkNumber < dataReady.length; chunkNumber++) {
            //marker
            System.arraycopy(chunkMarker, 0, dataReady[chunkNumber], 0, chunkMarkerLength);

            int[] encodableData = new int[chunkPacketNumberLength + chunkDataLength];
            //packet number
            int[] number = bytesToInts(new byte[]{chunkNumber});
            System.arraycopy(number, 0, encodableData, 0, chunkPacketNumberLength);
            //data
            for (int i = 0; i < chunkDataLength; i++) {
                try {
                    encodableData[chunkPacketNumberLength + i] = data[chunkNumber * chunkDataLength + i];
                } catch (Exception e) {
                }
            }
            int[] encodedData = hammingEncode(encodableData);
            System.arraycopy(encodedData, 0, dataReady[chunkNumber], chunkMarkerLength, encodedData.length);
        }

        //Print 2D array
        for (int i = 0; i < chunkLength; i++) {
            if (i % 8 == 0) {
                for (int a = 0; a < dataLength; a++) {
                }
            }
            for (int a = 0; a < dataReady.length; a++) {
            }
        }

        int dataStored = 0;
        int[] dataArray = new int[dataCapacity];

        int[][] shuffledData = ShuffleArray(dataReady);
        while (dataStored < dataCapacity) {
            shuffledData = ShuffleArray(shuffledData);

            try {
                for (int a = 0; a < shuffledData.length; a++) {
                    for (int b = 0; b < chunkLength; b++) {
                        dataArray[a * chunkLength + b + dataStored] = shuffledData[a][b];
                    }
                }
            } catch (Exception e) {
            }
            dataStored += shuffledData.length * chunkLength;
        }

        int[] dataFinal = dataArray;
        return dataFinal;
    }

    int[] decompose(int[] data) {

        //ArrayList<ArrayList<Integer>> chunks = new ArrayList<>();
        ArrayList<int[]> chunks = new ArrayList<>();

        int lastPosition = ArrayIndexOf(chunkMarker, data, 0);
        int currentPosition = lastPosition;

        while (lastPosition < data.length && lastPosition != -1 && currentPosition != -1) {
            currentPosition = ArrayIndexOf(chunkMarker, data, lastPosition + 1);
            if (currentPosition - lastPosition == chunkLength) {
                //copy as correct sizes
                chunks.add(Arrays.copyOfRange(data, lastPosition, currentPosition));
                lastPosition = currentPosition;
            } else {
                if (currentPosition == -1) {
                    break;
                }
                if (currentPosition - lastPosition < chunkLength) {
                    int tempPosition = ArrayIndexOf(chunkMarker, data, currentPosition + 1);
                    while (tempPosition - lastPosition < chunkLength && lastPosition != -1 && currentPosition != -1 && tempPosition != -1) {
                        tempPosition = ArrayIndexOf(chunkMarker, data, tempPosition + 1);
                    }
                    if (tempPosition == -1) {
                        break;
                    }
                    if (tempPosition - lastPosition == chunkLength) {
                        //copy as correct size
                        chunks.add(Arrays.copyOfRange(data, lastPosition, tempPosition));
                        lastPosition = tempPosition;
                    } else if (tempPosition - lastPosition > chunkLength) {
                        //copy as correct size
                        //chunks.add(Arrays.copyOfRange(data, lastPosition, currentPosition));
                        lastPosition = currentPosition;
                    }
                } else if (currentPosition - lastPosition > chunkLength) {
                    lastPosition = currentPosition;
                }
            }

        }
        if (data.length - lastPosition == chunkLength) {
            chunks.add(Arrays.copyOfRange(data, lastPosition, data.length));
        }

        for (int i = 0; i < chunks.get(0).length; i++) {
            if (i % 8 == 0) {
                for (int a = 0; a < chunks.size(); a++) {
                }
            }
            for (int a = 0; a < chunks.size(); a++) {
            }
        }

        //DECODE DATA HERE
        //max nuber of packets
        int max = 0;
        for (int a = 0; a < chunks.size(); a++) {
            int[] chunkFull = chunks.get(a);

            //get package number-----------------------------------------------------------------------------------------------------------------------------------------------------
            int[] first = new int[chunkPacketNumberLength * 2];
            System.arraycopy(chunkFull, chunkMarkerLength, first, 0, chunkPacketNumberLength * 2);
            int[] firstDecoded = hammingDecode(first);
            byte val = intsToBytes(firstDecoded)[0];
            if (val > max) {
                max = val;
            }
        }

        //sort data to repetetive array
        ArrayList<int[]>[] dataRetrieved = (ArrayList<int[]>[]) new ArrayList[max + 1];
        for (int i = 0; i < max + 1; i++) {
            dataRetrieved[i] = new ArrayList<int[]>();
        }

        for (int a = 0; a < chunks.size(); a++) {
            int[] chunkFull = chunks.get(a);

            //get package number-----------------------------------------------------------------------------------------------------------------------------------------------------
            int[] first = new int[chunkPacketNumberLength * 2];
            System.arraycopy(chunkFull, chunkMarkerLength, first, 0, chunkPacketNumberLength * 2);
            int[] firstDecoded = hammingDecode(first);
            byte val = intsToBytes(firstDecoded)[0];

            int[] chunkData = new int[chunkDataLength * 2];
            System.arraycopy(chunkFull, chunkMarkerLength + chunkPacketNumberLength * 2, chunkData, 0, chunkDataLength * 2);
            int[] chunkDataDecoded = hammingDecode(chunkData);
            try {
                dataRetrieved[val].add(chunkDataDecoded);
            } catch (Exception e) {
            }

        }

        int[] dataFinal = new int[dataRetrieved.length * chunkDataLength];
        //place data in the array
        for (int a = 0; a < dataRetrieved.length; a++) {
            for (int b = 0; b < chunkDataLength; b++) {
                try {
                    int one = 0;
                    int zero = 0;
                    for (int c = 0; c < dataRetrieved[a].size(); c++) {
                        if (dataRetrieved[a].get(c)[b] == 1) {
                            one += 1;
                        }
                        if (dataRetrieved[a].get(c)[b] == 0) {
                            zero += 1;
                        }
                    }
                    int value = 0;
                    if (one > zero) {
                        value = 1;
                    } else {
                        value = 0;
                    }
                    dataFinal[a * chunkDataLength + b] = value;
                } catch (Exception e) {
                }
            }
        }

//        int[] dataFinal = new int[chunks.size() * chunkDataLength];
//        //place data in the array
//        for (int a = 0; a < chunks.size(); a++) {
//            for (int b = 0; b < chunkDataLength; b++) {
//                try {
//                    
//                    dataFinal[a * chunkDataLength + b] = dataRetrieved[a].get(0)[b];
//                } catch (Exception e) {
//                }
//            }
//        }
        return dataFinal;
    }

    //https://en.wikipedia.org/wiki/Hamming%287,4%29
    //needs finishing
    int[] hammingEncode(int[] data) {
        if (data.length % 4 != 0) {
            return null;
        }

        int[] encodedChunkFinal = new int[data.length * 2];
        for (int i = 0; i < data.length / 4; i++) {
            int[] initialChunk = Arrays.copyOfRange(data, i * 4, i * 4 + 4);
            int[] encodedChunk = new int[8];

//0000
            if (Arrays.equals(new int[]{0, 0, 0, 0}, initialChunk)) {
                encodedChunk = new int[]{0, 0, 0, 0, 0, 0, 0, 0};
//1000
            } else if (Arrays.equals(new int[]{1, 0, 0, 0}, initialChunk)) {
                encodedChunk = new int[]{1, 1, 1, 0, 0, 0, 0, 1};
//0100
            } else if (Arrays.equals(new int[]{0, 1, 0, 0}, initialChunk)) {
                encodedChunk = new int[]{1, 0, 0, 1, 1, 0, 0, 1};
//1100
            } else if (Arrays.equals(new int[]{1, 1, 0, 0}, initialChunk)) {
                encodedChunk = new int[]{0, 1, 1, 1, 1, 0, 0, 0};
//0010
            } else if (Arrays.equals(new int[]{0, 0, 1, 0}, initialChunk)) {
                encodedChunk = new int[]{0, 1, 0, 1, 0, 1, 0, 1};
//1010
            } else if (Arrays.equals(new int[]{1, 0, 1, 0}, initialChunk)) {
                encodedChunk = new int[]{1, 0, 1, 1, 0, 1, 0, 0};
//0110
            } else if (Arrays.equals(new int[]{0, 1, 1, 0}, initialChunk)) {
                encodedChunk = new int[]{1, 1, 0, 0, 1, 1, 0, 0};
//1110
            } else if (Arrays.equals(new int[]{1, 1, 1, 0}, initialChunk)) {
                encodedChunk = new int[]{0, 0, 1, 0, 1, 1, 0, 1};
//0001
            } else if (Arrays.equals(new int[]{0, 0, 0, 1}, initialChunk)) {
                encodedChunk = new int[]{1, 1, 0, 1, 0, 0, 1, 0};
//1001
            } else if (Arrays.equals(new int[]{1, 0, 0, 1}, initialChunk)) {
                encodedChunk = new int[]{0, 0, 1, 1, 0, 0, 1, 1};
//0101
            } else if (Arrays.equals(new int[]{0, 1, 0, 1}, initialChunk)) {
                encodedChunk = new int[]{0, 1, 0, 0, 1, 0, 1, 1};
//1101
            } else if (Arrays.equals(new int[]{1, 1, 0, 1}, initialChunk)) {
                encodedChunk = new int[]{1, 0, 1, 0, 1, 0, 1, 0};
//0011
            } else if (Arrays.equals(new int[]{0, 0, 1, 1}, initialChunk)) {
                encodedChunk = new int[]{1, 0, 0, 0, 0, 1, 1, 1};
//1011
            } else if (Arrays.equals(new int[]{1, 0, 1, 1}, initialChunk)) {
                encodedChunk = new int[]{0, 1, 1, 0, 0, 1, 1, 0};
//0111
            } else if (Arrays.equals(new int[]{0, 1, 1, 1}, initialChunk)) {
                encodedChunk = new int[]{0, 0, 0, 1, 1, 1, 1, 0};
//1111
            } else if (Arrays.equals(new int[]{1, 1, 1, 1}, initialChunk)) {
                encodedChunk = new int[]{1, 1, 1, 1, 1, 1, 1, 1};

            }
            System.arraycopy(encodedChunk, 0, encodedChunkFinal, i * 8, 8);
        }
        return encodedChunkFinal;
    }

    int[][] hammingTable = new int[][]{
        {0, 0, 0, 0, 0, 0, 0, 0},//0000
        {1, 1, 0, 1, 0, 0, 1, 0},//0001
        {0, 1, 0, 1, 0, 1, 0, 1},//0010
        {1, 0, 0, 0, 0, 1, 1, 1},//0011
        {1, 0, 0, 1, 1, 0, 0, 1},//0100
        {0, 1, 0, 0, 1, 0, 1, 1},//0101
        {1, 1, 0, 0, 1, 1, 0, 0},//0110
        {0, 0, 0, 1, 1, 1, 1, 0},//0111
        {1, 1, 1, 0, 0, 0, 0, 1},//1000
        {0, 0, 1, 1, 0, 0, 1, 1},//1001
        {1, 0, 1, 1, 0, 1, 0, 0},//1010
        {0, 1, 1, 0, 0, 1, 1, 0},//1011
        {0, 1, 1, 1, 1, 0, 0, 0},//1100
        {1, 0, 1, 0, 1, 0, 1, 0},//1101
        {0, 0, 1, 0, 1, 1, 0, 1},//1110
        {1, 1, 1, 1, 1, 1, 1, 1}//1111
    };

    int[] hammingDecode(int[] data) {
        if (data.length % 8 != 0) {
            return null;
        }

        int[] decodedChunkFinal = new int[data.length / 2];
        for (int i = 0; i < data.length / 8; i++) {
            int[] initialChunk = Arrays.copyOfRange(data, i * 8, i * 8 + 8);
            int[] decodedChunk = new int[4];

            int matchIndex = 0;
            int highestMatch = 0;
            for (int a = 0; a < 16; a++) {
                int matches = 0;
                for (int b = 0; b < 8; b++) {
                    if (hammingTable[a][b] == initialChunk[b]) {
                        matches++;
                    }
                }
                if (matches > highestMatch) {
                    matchIndex = a;
                    highestMatch = matches;
                }
            }

            if (highestMatch == 7 || highestMatch == 8) {
                int[] bit = bytesToInts(new byte[]{(byte) matchIndex});
                decodedChunk = new int[]{bit[4], bit[5], bit[6], bit[7]};
            } else {
                //just guess between two values
                decodedChunk = bytesToInts(new byte[]{(byte) matchIndex});
            }

            System.arraycopy(decodedChunk, 0, decodedChunkFinal, i * 4, 4);
        }
        return decodedChunkFinal;
    }
//    
//    
//    
//    
//    
//    
//    
//    
//    
//    
//    
//    
//    
//    
//    
//    
//    

    int ArrayIndexOf(int[] searchTerm, int[] data, int begining) {
        if (data.length < searchTerm.length) {
            return -1;
        }
        for (int a = begining; a < data.length - searchTerm.length; a++) {
            boolean found = true;
            for (int b = 0; b < searchTerm.length; b++) {
                if (data[a + b] == searchTerm[b]) {

                } else {
                    found = false;
                    break;
                }
            }
            if (found) {
                return a;
            }
        }
        return -1;
    }

    int[][] ShuffleArray(int[][] array) {
        int[] temp;
        int index;
        Random random = new Random();
        for (int i = array.length - 1; i > 0; i--) {
            index = random.nextInt(i + 1);
            temp = array[index];
            array[index] = array[i];
            array[i] = temp;
        }
        return array;
    }

    int[] bytesToInts(byte[] data) {
        int[] dataInt = new int[data.length * 8];
        dataInt = new int[data.length * 8];
        for (int a = 0; a < data.length; a++) {
            byte byteTemp = data[a];
            for (int b = 0; b < 8; b++) {
                dataInt[a * 8 + (7 - b)] = (byteTemp >>> (b)) & 1;
            }
        }
        return dataInt;
    }

    byte[] intsToBytes(int[] data) {
        byte[] bytes = new byte[data.length / 8];
        byte temp = 0;
        for (int a = 0; a < data.length; a += 8) {
            temp = 0;
            for (int i = 0; i < 8; i++) {
                temp = (byte) ((temp << 1) | (byte) data[a + i]);
            }
            bytes[a / 8] = temp;
        }
        return bytes;
    }

    String IntsToString(int[] data) {
        char[] chars = new char[data.length];
        for (int a = 0; a < data.length; a++) {
            chars[a] = (data[a] + "").toCharArray()[0];
        }
        return String.copyValueOf(chars);
    }

    public void repairData() {

    }
}

package tk.steganograsaurus.steg;

import com.sun.imageio.plugins.png.PNGImageReader;
import com.sun.imageio.plugins.png.PNGImageReaderSpi;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;

/**
 * Created by Adomas on 2016-01-30.
 */

import javax.imageio.ImageReader;
import javax.imageio.spi.ImageReaderSpi;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class ImageContainer extends FileData {

    private int[][] dataRGB;
    private YCbCr[][] data;

    public ImageContainer(byte[] imageData) {
        super(imageData, "");

        BufferedImage img;
        ByteArrayInputStream bais = new ByteArrayInputStream(imageData);
        try {
            img = ImageIO.read(bais);
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

        dataRGB = ImageContainer.convertTo2DWithoutUsingGetRGB(img);
        data = new YCbCr[dataRGB.length][dataRGB[0].length];
        updateYCrCbBitmap();
    }

    public int getWidth() {
        return data[0].length;
    }

    public int getHeight() {
        return data.length;
    }

    public int getPixelLuminosity(int x, int y) {
        return data[y][x].Y;
    }

    public void setPixelLuminosity(int x, int y, int luma) {
        if (luma < 0 || luma > 255)
            return;
        data[y][x].Y = luma;

    }

    private static int[][] convertTo2DWithoutUsingGetRGB(BufferedImage image) {

        final byte[] pixels = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
        final int width = image.getWidth();
        final int height = image.getHeight();
        final boolean hasAlphaChannel = image.getAlphaRaster() != null;

        int[][] result = new int[height][width];
        if (hasAlphaChannel) {
            final int pixelLength = 4;
            for (int pixel = 0, row = 0, col = 0; pixel < pixels.length; pixel += pixelLength) {
                int argb = 0;
                argb += (((int) pixels[pixel] & 0xff) << 24); // alpha
                argb += ((int) pixels[pixel + 1] & 0xff); // blue
                argb += (((int) pixels[pixel + 2] & 0xff) << 8); // green
                argb += (((int) pixels[pixel + 3] & 0xff) << 16); // red
                result[row][col] = argb;
                col++;
                if (col == width) {
                    col = 0;
                    row++;
                }
            }
        } else {
            final int pixelLength = 3;
            for (int pixel = 0, row = 0, col = 0; pixel < pixels.length; pixel += pixelLength) {
                int argb = 0;
                argb += -16777216; // 255 alpha
                argb += ((int) pixels[pixel] & 0xff); // blue
                argb += (((int) pixels[pixel + 1] & 0xff) << 8); // green
                argb += (((int) pixels[pixel + 2] & 0xff) << 16); // red
                result[row][col] = argb;
                col++;
                if (col == width) {
                    col = 0;
                    row++;
                }
            }
        }

        return result;
    }


    private void updateYCrCbBitmap() {
        for (int i = 0; i < dataRGB.length; i++) {
            for (int ii = 0; ii < dataRGB[i].length; ii++) {
                data[i][ii] = new YCbCr(new Color(dataRGB[i][ii]));
            }
        }
    }

    private void updateRGBBitmap() {
        for (int i = 0; i < data.length; i++) {
            for (int ii = 0; ii < data[i].length; ii++) {
                dataRGB[i][ii] = data[i][ii].toRGB();
            }
        }
    }

    @Override
    public byte[] getByteStream() {
        updateByteStream();
        return super.getByteStream();
    }

    private void updateByteStream() {
        updateRGBBitmap();
        BufferedImage img = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_RGB);
        for (int i = 0; i < dataRGB.length; i++) {
            for (int ii = 0; ii < dataRGB[i].length; ii++) {
                img.setRGB(ii, i, dataRGB[i][ii]);
            }
        }
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            ImageIO.write(img, "PNG", baos);
        } catch (IOException e) {
            e.printStackTrace();
        }
        super.data = baos.toByteArray();
    }

    public void logRGBFile() {
        updateRGBBitmap();
        BufferedImage img = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_RGB);
        for (int i = 0; i < dataRGB.length; i++) {
            for (int ii = 0; ii < dataRGB[i].length; ii++) {
                img.setRGB(ii, i, dataRGB[i][ii]);
            }
        }
        File f = new File("temp.png");
        try {
            ImageIO.write(img, "PNG", f);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private class YCbCr {
        private int Y;
        private int Cb;
        private int Cr;

        public YCbCr(Color c) {
            int R = c.getRed();
            int G = c.getGreen();
            int B = c.getBlue();

            Y = (int) Math.round(0.299 * R + 0.587 * G + 0.114 * B);
            Cb = (int) Math.round(-0.169 * R - 0.331 * G + 0.499 * B + 128);
            Cr = (int) Math.round(0.499 * R - 0.418 * G - 0.0813 * B + 128);

        }

        private int clamp(int i) {
            if (i < 0)
                return 0;
            if (i > 255)
                return 255;
            else
                return i;
        }

        public int toRGB() {
            int R = clamp((int) Math.round(Y + 1.402 * (Cr - 128)));
            int G = clamp((int) Math.round(Y - 0.344 * (Cb - 128) - 0.714 * (Cr - 128)));
            int B = clamp((int) Math.round(Y + 1.772 * (Cb - 128)));

            return (R << 16) | (G << 8) | B;
        }

    }
}


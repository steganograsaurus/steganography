package tk.steganograsaurus.steg;

import java.util.ServiceLoader;

public abstract class StegFactory {
    static ServiceLoader<StegFactory> factoryLoader
        = ServiceLoader.load(StegFactory.class);

    public abstract ISteg buildSteg(FileData data);
}

package tk.steganograsaurus.steg;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.ColorConvertOp;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Adomas on 2016-01-30.
 */
public class ImageSteg implements ISteg {

    private ImageContainer image;

    private int pixelPerChunk = 3;
    private int chunkCapacity = 1;
    private int treshhold = 2;

    public ImageSteg(ImageContainer data) {
        image = data;
        extract = new int[image.getHeight()][image.getWidth()];
        horizontalLines = new int[image.getHeight()];
        verticalLines = new int[image.getWidth()];
    }

    @Override
    public int capacity() {
        return (image.getWidth() / pixelPerChunk) * (image.getHeight() / pixelPerChunk) * chunkCapacity;
    }

    @Override
    public ImageContainer encode(FileData data) {
        FileDataForImage fd = new FileDataForImage(data.getByteStream(), this.capacity());
        fd.Encode();
        int count = 0;
        byte[] d = fd.getByteStream();
        for (int r = 0; r < image.getHeight(); r += pixelPerChunk) {
            for (int c = 0; c < image.getWidth(); c += pixelPerChunk) {
                if (count == d.length)
                    break;

                encodeChunk(c, r, (int) d[count]);
                count++;
            }
        }
        image.logRGBFile();
        return image;
    }

    private void encodeChunk(int x, int y, int value) {
        int ajustment, ca;

        while (true) {
            ajustment = getAdjustmentDiff(average(x, y) - getChunkCenter(x, y), value);
            if (ajustment == 0)
                break;
            ca = ajustment / 2;
            updateChunkCenterLuminosity(x, y, ca);
            updateChunkBorderLuminosity(x, y, ca - ajustment);
        }
    }

    private int getAdjustmentDiff(int diff, int value) {
        double temp = (diff + (value == 1 ? -1 : 1) * treshhold) / (4d * treshhold);
        int minN = (int) Math.floor(temp);
        int maxN = (int) Math.ceil(temp);

        int minA = (4 * minN + (value == 1 ? 1 : -1)) * treshhold - diff;
        int maxA = (4 * maxN + (value == 1 ? 1 : -1)) * treshhold - diff;
        if (Math.abs(minA) < Math.abs(maxA))
            return minA;
        else
            return maxA;
    }


    private int getChunkCenter(int x, int y) {
        int v =
                image.getPixelLuminosity(x + pixelPerChunk / 2, y + pixelPerChunk / 2);
        return v;
    }

    private void updateChunkCenterLuminosity(int x, int y, int v) {
        int cx = x + pixelPerChunk / 2;
        int cy = y + pixelPerChunk / 2;
        image.setPixelLuminosity(cx, cy, image.getPixelLuminosity(cx, cy) + v);

    }

    private void updateChunkBorderLuminosity(int x, int y, int v) {
        int cx = x + pixelPerChunk / 2;
        int cy = y + pixelPerChunk / 2;

        for (int i = 0; i < pixelPerChunk; i++) {
            for (int ii = 0; ii < pixelPerChunk; ii++) {
                if (!(cx == x + i && cy == y + ii))
                    image.setPixelLuminosity(x + i, y + ii, image.getPixelLuminosity(x + i, y + ii) + v);
            }
        }
    }

    private int average(int x, int y) {
        int average = 0;
        int cx = x + pixelPerChunk / 2;
        int cy = y + pixelPerChunk / 2;

        for (int i = 0; i < pixelPerChunk; i++) {
            for (int ii = 0; ii < pixelPerChunk; ii++) {
                if (!(cx == x + i && cy == y + ii))
                    average += image.getPixelLuminosity(x + i, y + ii);
            }
        }
        average /= pixelPerChunk * pixelPerChunk - 1;
        return average;
    }

    private int[][] extract;
    private int cosAccepted = 60;

    private int[] horizontalLines;
    private int[] verticalLines;

    private int spaceToDeleteLine = 4;

    @Override
    public FileData decode() {
        for (int r = 0; r < image.getHeight() - pixelPerChunk + 1; r++) {
            for (int c = 0; c < image.getWidth() - pixelPerChunk + 1; c++) {
                extract[r + pixelPerChunk / 2][c + pixelPerChunk / 2] = getCosValue(c, r);
            }
        }

        clearLines();
        clearLines();
//        logExtract();
        ArrayList<Byte> d = new ArrayList<>();

        for (int i = 0; i < extract.length; i++) {

            for (int ii = 0; ii < extract[i].length; ii++) {
                if (Math.abs(extract[i][ii]) >= cosAccepted) {
                    if (extract[i][ii] < 0) {
                        d.add((byte) 0);
                    } else {
                        d.add((byte) 1);
                    }
                }
            }
        }
        byte[] bd = new byte[d.size()];
        for (int i = 0; i < d.size(); i++) {
            bd[i] = d.get(i);
        }
        FileDataForImage fd = new FileDataForImage(bd, this.capacity());
        return fd.Decode();
    }

    private void clearLines() {
        horizontalLines = new int[image.getHeight()];
        verticalLines = new int[image.getWidth()];

        int v;
        boolean space = false;
        int count = 0;
        int m = 0;
        for (int r = 0; r < image.getHeight(); r++) {
            m = 0;
            count = 0;
            for (int c = 0; c < image.getWidth(); c++) {
                v = extract[r][c];
                if (Math.abs(v) >= cosAccepted) {
                    if (space) {
                        m = Math.max(m, count);
                    }
                    count = 0;
                    space = false;
                } else {
                    space = true;
                    count++;
                }

            }

            horizontalLines[r] = Math.max(m, count);

        }
        for (int c = 0; c < image.getWidth(); c++) {
            m = 0;
            count = 0;
            for (int r = 0; r < image.getHeight(); r++) {
                v = extract[r][c];
                if (Math.abs(v) >= cosAccepted) {
                    if (space) {
                        m = Math.max(m, count);
                    }
                    count = 0;
                    space = false;
                } else {
                    space = true;
                    count++;
                }
            }
            verticalLines[c] = Math.max(m, count);

        }


        for (int i = 0; i < horizontalLines.length; i++) {

            if (horizontalLines[i] > spaceToDeleteLine) {
                for (int ii = 0; ii < verticalLines.length; ii++) {
                    extract[i][ii] = 0;
                }
            }
        }
        for (int i = 0; i < verticalLines.length; i++) {

            if (verticalLines[i] > spaceToDeleteLine) {
                for (int ii = 0; ii < horizontalLines.length; ii++) {
                    extract[ii][i] = 0;
                }
            }
        }

    }


    private void logExtract() {
        BufferedImage img = new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.TYPE_INT_RGB);
        for (int i = 0; i < extract.length; i++) {

            for (int ii = 0; ii < extract[i].length; ii++) {
                if (Math.abs(extract[i][ii]) >= cosAccepted) {
                    if (extract[i][ii] < 0) {
                        img.setRGB(ii, i, Color.RED.getRGB());
                    } else {
                        img.setRGB(ii, i, Color.BLUE.getRGB());
                    }
                } else {
                    img.setRGB(ii, i, Color.WHITE.getRGB());
                }
            }
        }
        File f = new File("decodeTest.png");
        try {
            ImageIO.write(img, "PNG", f);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private int getCosValue(int x, int y) {
        int average = average(x, y);
        int center = getChunkCenter(x, y);
        return (int) (100 * Math.sin((average - center) * 2 * Math.PI / (4 * treshhold)));
    }
}

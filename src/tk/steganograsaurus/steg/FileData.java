package tk.steganograsaurus.steg;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.net.URLConnection;
import java.nio.file.Files;

/**
 * Immutable object representing arbitrary data in some file format
 */
public class FileData {
    byte[] data;
    private String mimetype;

    public FileData(byte[] data, String mimetype) {
        this.data = data;
        this.mimetype = mimetype;
    }

    public byte[] getByteStream() {
        return this.data;
    }

    public String getMimetype() {
        return this.mimetype;
    }

    public static FileData fromBytes(byte[] bytes) throws IOException {
        return new FileData(
            bytes,
            URLConnection.guessContentTypeFromStream(new ByteArrayInputStream(bytes))
        );
    }

    public static FileData fromFile(File file) throws IOException {
        byte[] bytes = Files.readAllBytes(file.toPath());
        return FileData.fromBytes(bytes);
    }
}

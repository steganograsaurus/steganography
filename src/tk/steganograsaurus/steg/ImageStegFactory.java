package tk.steganograsaurus.steg;

import java.util.zip.DataFormatException;

public class ImageStegFactory extends StegFactory {
    public ImageSteg buildSteg(FileData data) {
//        try {
            ImageContainer container = new ImageContainer(data.getByteStream());
            return new ImageSteg(container);
//        } catch (DataFormatException e) {
//            return null;
//        }
    }
}

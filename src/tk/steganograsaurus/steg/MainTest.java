package tk.steganograsaurus.steg;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;
import tk.steganograsaurus.util.CLI;

/**
 * Created by Adomas on 2016-01-30.
 */
public class MainTest {

    public static void main(String[] args) {

        try {
            BufferedImage img = ImageIO.read(new File("temp.png"));
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(img, "PNG", baos);
            byte[] bytes = baos.toByteArray();
            ImageSteg steg = new ImageStegFactory().buildSteg(new ImageContainer(bytes));
            //steg.capacity()

//            //Povilo
//            Path path = Paths.get("C:\\Users\\Povilas\\Desktop\\TestData.txt");
//            byte[] data;
//            try {
//                data = Files.readAllBytes(path);
//                FileDataForImage imageData = new FileDataForImage(data, steg.capacity());
//                imageData.Encode();
//                steg.encode(imageData);
//            } catch (IOException ex) {
//                Logger.getLogger(MainTest.class.getName()).log(Level.SEVERE, null, ex);
//            }

            FileData fd = steg.decode();

            FileDataForImage fData = new FileDataForImage(fd.getByteStream(), steg.capacity());
            fData.Decode();
            
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}

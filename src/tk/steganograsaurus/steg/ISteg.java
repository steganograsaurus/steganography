package tk.steganograsaurus.steg;

public interface ISteg {
    public int capacity();

    public FileData encode(FileData data);

    public FileData decode();

    public static ISteg buildSteg(FileData data) {
        for (StegFactory factory : StegFactory.factoryLoader) {
            ISteg steg = factory.buildSteg(data);
            if (steg != null) {
                return steg;
            }
        }
        return null;
    }
}

